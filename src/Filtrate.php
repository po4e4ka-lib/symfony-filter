<?php

namespace Po4e4ka\SymfonyFilter;


use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\NativeQuery;
use Doctrine\ORM\Query;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\HttpKernel\Exception\UnprocessableEntityHttpException;
use Throwable;

class Filtrate
{
    private array $filtrateArray = [];

    public function filtrate(
        QueryBuilder $queryBuilder,
        ?array       $filtrateArray = null,
        ?int         $limit = null,
        ?int         $offset = null
    ): array
    {
        $this->filtrateArray = $filtrateArray ?? [];
        $this->checkValidFiltrateArray();
        [$rsm, $columnAliasArray] = $this->getSelectAliasFromQueryBuilderAndCreateRSM($queryBuilder);
        if ($rsm) {
            $queryBuilderSql = $queryBuilder->getQuery()->getSQL();
            $this->checkAliasCountInQueryAndUpdateAliasArrayAndRSM($queryBuilderSql, $columnAliasArray, $rsm);

            $resultSql = 'SELECT * FROM (' . $queryBuilderSql . ') as sub ';

            $parameterArrayFromFilter = $this->addFiltersToSQLAndMakeParameterArray($resultSql, $columnAliasArray);
            $this->addLimit($resultSql, $limit);
            $this->addOffset($resultSql, $offset);
            $nativeSql = $queryBuilder->getEntityManager()->createNativeQuery($resultSql, $rsm);
            $this->addParameterFromQueryBuilderToSql($nativeSql, $queryBuilder->getParameters(), $parameterArrayFromFilter);
            return $nativeSql->getResult();
        }


        $alias = $queryBuilder->getAllAliases()[0];
        $parameterArrayFromFilter = [];
        $i = 1;
        foreach ($this->filtrateArray as $filter) {
            $filter[0] = $this->snakeToCamel($filter[0]);
            if (strpos($filter[0], '.')) {
                $this->fieldLevelExplode($filter[0], $queryBuilder, $alias);
            }
            if (($filter[1] == 'is' or $filter[1] == 'is_not') and $filter[2] == 'null') {
                $condition = $alias . '.' . $filter[0] . ' ' . $filter[1] == 'is' ? 'is' : 'is not' . ' null ';
            }
            elseif ($filter[1] == '~') {
                $this->checkRegSimbols($filter[2]);
                $condition = "REGEXP(" . $alias . '.' . $filter[0] . ', ?' . $i++ . ' ) = true';
                $parameterArrayFromFilter[] = $filter[2];
            } else {
                $condition = $alias . '.' . $filter[0] . ' ' . $filter[1] . ' ?' . $i++ . ' ';
                $parameterArrayFromFilter[] = $filter[2];
            }
            $queryBuilder->andWhere($condition);
        }
        $i = 1;
        foreach ($parameterArrayFromFilter as $value) {
            $queryBuilder->setParameter($i++, $value);
        }
        return $this->limitAndOffset(
            $queryBuilder, $limit, $offset
        )->getQuery()->getResult();
    }

    private function fieldLevelExplode(string &$filterField, QueryBuilder $queryBuilder, string &$alias)
    {
        $levelFilter = explode('.', $filterField);
        while (count($levelFilter) !== 1) {
            if (in_array($levelFilter[0], $queryBuilder->getAllAliases())) {
                $alias = $levelFilter[0];
                array_shift($levelFilter);
            } else {
                $queryBuilder->leftJoin($alias . '.' . $levelFilter[0], $levelFilter[0]);
            }
        }
        $filterField = $levelFilter[0];
    }

    private function snakeToCamel($string, $capitalizeFirstCharacter = false)
    {
        $str = str_replace('_', '', ucwords($string, '_'));

        if (!$capitalizeFirstCharacter) {
            $str = lcfirst($str);
        }
        return $str;
    }

    private function addParameterFromQueryBuilderToSql(NativeQuery &$nativeSql, ArrayCollection $replaceParameters, array $filterParameters)
    {
        $i = 1;
        $parameterArray = [];
        foreach ($replaceParameters as $parameter) {
            $parameterArray[] = $parameter->getValue();
        }
        foreach ($filterParameters as $parameter) {
            $parameterArray[] = $parameter;
        }
        foreach ($parameterArray as $value) {
            if (is_array($value)) {
                $value = '\'' . implode('\', \'', $value) . '\'';
            }
            $nativeSql->setParameter($i++, $value);
        }
    }

    private function camelToSnake($string)
    {
        $str = strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $string));;

        return $str;
    }

    private function checkValidFiltrateArray(): void
    {
        if ($this->filtrateArray === []) {
            return;
        }
        if (count($this->filtrateArray) % 3 !== 0) {
            $this->validationError('Необходимо указывать по 3 элемента (поле, условие, значение)');
        }
        $this->filtrateArray = array_chunk($this->filtrateArray ?? [], 3);
        try {
            foreach ($this->filtrateArray as &$filter) {

                if (!is_string($filter[0])) {
                    $this->validationError('Поле для фильрации должно быть строкой');
                }
                $conditionValidArray = ['>', '<', '=', '<=', '>=', '~', '<>', '!=', 'is', 'is_not'];
                if (!(in_array($filter[1], $conditionValidArray))) {
                    $this->validationError('Условие должно быть одним из ' . implode(', ', $conditionValidArray));
                }
                if (!is_string($filter[2])) {
                    $this->validationError('Поле значение должно быть строкой');
                }
            }
        } catch (Throwable $e) {
            $this->validationError($e->getMessage());
        }
    }

    private function checkRegSimbols(string &$filter) {
        $regSimbols = ['\\', '?', '*', '+', '|', '[', ']', '{', '}', '(', ')', '.'];

        if (is_string($filter)) {
            foreach ($regSimbols as $simbol) {
                $filter = str_replace($simbol, '\\' . $simbol, $filter);
            }
        }
    }

    private function validationError(string $message)
    {
        throw new UnprocessableEntityHttpException('Ошибка формата фильтра: ' . $message);
    }


    public function limitAndOffset(QueryBuilder $queryBuilder, ?int $limit = null, ?int $offset = null): QueryBuilder
    {
        return $queryBuilder
            ->setMaxResults($limit)
            ->setFirstResult($offset);
    }

    public function paginate(QueryBuilder $queryBuilder, ?int $limit = null, int $page = 1): array
    {
        $resultRowCount = count($queryBuilder->getQuery()->getResult());
        $queryBuilder
            ->setMaxResults($limit)
            ->setFirstResult(($page - 1) * $limit);
        $pagesCount = (int)ceil($resultRowCount / $limit);
        return [
            "pages_count" => $pagesCount,
            "current_page_number" => $page ?? 1,
            "row_count" => $resultRowCount,
            "data" => $queryBuilder->getQuery()->getResult()
        ];
    }

    private function getSelectAliasFromQueryBuilderAndCreateRSM(QueryBuilder $queryBuilder)
    {
        // TODO Если нет селекта
        $fieldAliasArray = $queryBuilder->getDQLPart('select')[0]->getParts();
        preg_match_all('/[\r\n, ]*(?<tableField>[\w\d(]*\(?[\w\d]+\.(?<field>[\w\d]+)\)*)( as +(?<alias>[\w\d_]+))?/u',
            $fieldAliasArray[0], $fieldAliasArray);
        if (count($fieldAliasArray['tableField']) === 0) {
            return [false, false];
        }
        $index = 0;
        $rsm = new Query\ResultSetMapping();
        $aliasFieldArray = [];
        foreach ($fieldAliasArray['field'] as $item) {
            $item = $this->camelToSnake($item) . '_' . $index;
            if (preg_match('/(?<count>count)\([\w\d.()]+\)/u', $fieldAliasArray['tableField'][$index])) {
                $item = 'sclr_' . $index;
            }
            $rsm->typeMappings[$item] = 'string';
            if (empty($fieldAliasArray['alias'][$index]))
                $fieldAliasArray['alias'][$index] = $fieldAliasArray['field'][$index];
            $rsm->scalarMappings[$item] = $fieldAliasArray['alias'][$index];
            $aliasFieldArray[$fieldAliasArray['alias'][$index]] = $item;
            $index++;
        }
        return [$rsm, $aliasFieldArray];
    }

    private function addFiltersToSQLAndMakeParameterArray(string &$resultSql, array $columnAliasArray)
    {
        $index = 0;
        $parameters = [];
        foreach ($this->filtrateArray as $filter) {
            $filterField = $columnAliasArray[$filter[0]] ?? $filter[0];
            $condRight = $filter[2];
            if ($filter[1] == '~') {
                $this->checkRegSimbols($condRight);
                $filterField = $filterField . '::text';
            }            
            if (array_key_exists($condRight, $columnAliasArray)) {
                $condRight = $columnAliasArray[$condRight] . ' ';
            } elseif (($filter[1] == 'is' or $filter[1] == 'is_not') and $filter[2] == 'null') {
                $filter[1] = $filter[1] == 'is' ? 'is' : 'is not';
                $condRight = 'null ';
            } else {
                $parameters[] = $condRight;
                $condRight = '? ';
            }

            $filterString = ($index == 0 ? 'WHERE ' : 'AND ') . $filterField . ' ' . $filter[1] . ' ' . $condRight;

            $resultSql .= $filterString;
            $index++;
        }
        return $parameters;
    }

    private function addLimit(string &$resultSql, ?int $limit = null)
    {
        if ($limit !== null) {
            $resultSql .= ' LIMIT ' . $limit;
        }
    }

    private function addOffset(string &$resultSql, ?int $offset = null)
    {
        if ($offset !== null) {
            $resultSql .= ' OFFSET ' . $offset;
        }
    }

    private function checkAliasCountInQueryAndUpdateAliasArrayAndRSM(string $sql, array &$aliasArray, Query\ResultSetMapping &$rsm)
    {
        $findAlias = [];
        preg_match_all("/[\r\n, ]*(?<table>[\w\d_]*)\.(?<field>[\w\d]+)( ->> '(?<key>[\w\d]+)')? +AS +(?<alias>[\w\d_]+)/u", $sql, $findAlias);
        $findAliasCount = count($findAlias['alias']);
        $currentAliasCount = count($aliasArray);
        if ($findAliasCount === $currentAliasCount) {
            return;
        }
        $aliasArray = [];
        for ($index = $currentAliasCount; $index < $findAliasCount; $index++) {
            $aliasArray[$findAlias['key'][$index]] = $findAlias['alias'][$index];
            $rsm->scalarMappings[$findAlias['alias'][$index]] = $findAlias['key'][$index];
            $rsm->typeMappings[$findAlias['alias'][$index]] = "string";
        }
    }
}